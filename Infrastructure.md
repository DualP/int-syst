# Architecture nginx

## Schéma archi

* 2 server Apache
* 1 server reverse proxy nginx

![oups](Images/archi.PNG)

## Installation serveur Apache


 * On commence par installer le paquer httpd qui contient apache
 
```
sudo yum install httpd
```

 * Comme on est sous Centos7, apache est préconfigurer et a donc une page statique par défaut. Reste à la lancer :
 
```
sudo systemctl enable httpd
sudo systemctl start httpd
```

 * Petit bonus assez fun, on peut voir le site en mode texte directement dans le terminal avec le paquet Elinks :

```
sudo yum install elinks
```

 * Si ça marche pas comme c'est le cas pour moi, on ouvre le port 80 et 443 (http et https) dans le parefeu :

```
sudo firewall-cmd ––permanent ––add-port=80/tcp
sudo firewall-cmd ––permanent ––add-port=443/tcp
sudo firewall-cmd --reload
```

* Les deux serveur Apache fonctionnent correctement.


## Installation nginx

 * D'après ce que j'ai pu trouver, nginx me semle utile pour deux raisons:
   * Protéger la vie privée des utilisateurs
   * Plus de fluidité et donc de rapidité, en l'occurance avec le système de "log balancing" que l'on verra plus bas

 * On commence par installer le paquer nginx tout simplement

```
sudo yum update
sudo yum install nginx
```

 * Le lancement de nginx :

```
sudo systemctl start nginx
sudo systemctl enable nginx
```

 * On le vérifie avec :
 
```
sudo systemctl status nginx
```

 * Le status affiche la ligne :

```
Active: active (running) since Thu 2019-09-08 13:42:21 CEST; 42s ago
```

## Load balancing

### Qu'es-ce que c'est ?

Le load balancing ( répartiteur de charge en français ) réponds au besoins de pallier une charge trop importante d'un service en la répartissant entre plusieurs serveur.  
Ca permet aussi de mettre en place de la haute disponibilité, c'est à dire de pallier à une panne de serveur.  
Exemple: on mets deux fois le même site internet sur deux serveur différents.  
Le load balancing vas permettre de répartir le Traffic sur les deux serveurs afin de ne pas surchargé un des deux serveurs  
Mais si l'un des deux serveurs tombe en panne il vas redirigé tous le traffic sur le serveur qui est encore up de manière transparante.   
l'inconvégniant est juste une perte de performance du a un traffic plus important sur la même machine

![oups](Images/loadBalancing.png)

* Configurer nginx avec des instructions sur le type de connexions à écouter et sur leur redirection.  
Il faut pour ça créez un nouveau fichier de configuration :

```
sudo nano /etc/nginx/conf.d/load-balancer.conf
```

* Dans le fichier load-balancer.conf, on va définir deux segments, en "amont" et le "serveur" :

```
http {
   backend amont {
      serveur 10.1.0.101; 
      serveur 10.1.0.102;
      serveur 10.1.0.103;
   }

   # Ce serveur accepte tout le trafic sur le port 80 et le transmet à l'amont. 
   # Notez que le nom en amont et le proxy_pass doivent correspondre.

   serveur {
      écoutez 80; 

      emplacement / {
          proxy_pass http: // backend;
      }
   }
}
```