# Sauvegarde et restauration

## Sauvegarde

J'ai utilisé deux moyens différents pour sauvegarder mes données, les deux étant de les avoirs dans un disque dure externe :

### Première
 * Le premier j'ai enregistré tout mons OS, via le panneau de configuration Windows :
 
 * Pour le résupérer il y a tout simplement un bouton juste à côté pour restaurer la sauvegarde

![oups](Images/saveOS.PNG)

### Deuxième

 * La première solution est très interessante mais elle ne me permet de restaurer qu'un seul fichier.
 * Restaurer tout son OS pour récupérer son devoir de Maths c'est pas le plus opti...
 * Le drag and drop
 * J'ai enregistrer une copie de mes dossiers manuellement dans mon disque dure externe en plus de mon OS
 
## Avis personel

Ce sont des solutions de base mais utile. Je les maitrises et j'ai toujours mon disque dure avec mon ordinateur, donc ça ne me pose aucun problèmes.
 